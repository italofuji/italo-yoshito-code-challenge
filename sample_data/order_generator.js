/* 
    Generate Only Orders Sample Data

*/
import Products from '../src/models/products';
import Orders from '../src/models/orders';
import faker from 'faker';
import * as R from 'ramda';
import moment from 'moment';
import mongoose from 'mongoose';
import * as helpers from '../src/utils//geral_purpose_helpers';

// Credentials Mongodb
import * as config from '../src/config/_config';

// CONFIGURATION AREA
const RANGE_ORDER_DATE = {
    start: "2019-01-01",
    end: "2019-01-31"
}

if (RANGE_ORDER_DATE.start.isAfter(RANGE_ORDER_DATE.end)) {
    console.log("start date must be before end date.");
    process.exit();
}

const qtd_days = RANGE_ORDER_DATE.end.diff(RANGE_ORDER_DATE.start, 'days');
console.log(qtd_days);
// // Connect to database (Mongodb Atlas - Cloud)
mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true }).then(async (result) => {
    // If connection success, start the application
    console.log('connected to mongodb atlas.');

    Products.find({}).exec()
        .then((result) => {

            const orders = R.map((product) => ({
                code: '11' + `${product.code}`.padStart(3, '0'),
                purchase_date: helpers.get_random_date_between(RANGE_ORDER_DATE.start, RANGE_ORDER_DATE.end),
                buyer_name: faker.name.firstName(),
                order_status: helpers.get_random_element_from(['novo', 'aprovado','entregue','cancelado']),
                transportation_cost: helpers.get_random_arbitrary(50, 150),
                items: [
                    {
                        code: product.code,
                        quantity: product.quantity_in_stock,
                        value: product.value
                    }
                ]
            }), result);
            
            console.log(orders);

            Orders.insertMany(orders, (error, docs) => {
                if (!error) {
                    console.log("new orders created and inserted to collection.");
                    mongoose.connection.close();
                    console.log("connection closed");
                } else {
                    throw new Error(error);
                }
            })
        })
        .catch(error => {
            throw new Error("Problem to find Products.")
        });
    
}).catch((error) => {
    throw new Error(error);
});
