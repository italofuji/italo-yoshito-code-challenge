/* 
    Generate Only Products Sample Data

    Attention !

    It's generates only a limited number of data (configurable with ROW_QUANTITY constant), 
    it's limited to 100 registers due to that project
    uses a free version of mongodb atlas on the cloud.
*/
import Products from '../src/models/products';
import faker from 'faker';
import * as R from 'ramda';
import mongoose from 'mongoose';
import * as helpers from '../src/utils//geral_purpose_helpers';

// Credentials Mongodb
import * as config from '../src/config/_config';

// CONFIGURATION AREA
const ROW_QUANTITY = 30;
const STOCK_RANGE = {
    min: 0,
    max: 10
};

if (ROW_QUANTITY > 100) {
    console.log("ATTENTION ! SET ROW_QUANTITY TO BELOW 100.");
    process.exit();
}

// Connect to database (Mongodb Atlas - Cloud)
mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true }).then(async (result) => {
    // If connection success, start the application
    console.log('connected to mongodb atlas');

    const productGenerator = (index) => ({
        code: `${index}`.padStart(3, '0'),
        product_name: faker.commerce.productName(),
        description: faker.commerce.productAdjective(),
        quantity_in_stock: helpers.get_random_arbitrary(STOCK_RANGE.min, STOCK_RANGE.max),
        value: faker.commerce.price(),
        other_attributes: {
            color: faker.commerce.color(),
            material: faker.commerce.productMaterial()
        }
    })
    
    const products = R.map(productGenerator, R.range(1, ROW_QUANTITY + 1));
    
    Products.deleteMany({}, (error, removed) => {
        if (!error) {
            Products.insertMany(products, (error, docs) => {
                if (!error) {
                    console.log("new products created and inserted to collection.");
                    mongoose.connection.close();
                    console.log("connection closed");
                } else {
                    throw new Error(error);
                }
            })
        } else {
            throw new Error(error);
        }        
    });
    
}).catch((error) => {
    throw new Error(error);
});
