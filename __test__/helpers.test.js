import * as helpers from '../src/utils/geral_purpose_helpers';
import * as consts from './consts/samples';
import moment from 'moment';

describe('Testing Helpers Functions', () => {

    it('should return one element from given list. (function: get_random_element_from)', () => {
        const list = [1,2,3,4,5];
        const selected_element = helpers.get_random_element_from(list);
        expect(list).toContain(selected_element);
    });

    it('should return new date (Date) between 2 given dates. (function: get_random_date_between)', () => {
        let start = '2019-01-01';
        let end = '2019-01-31';

        let selected_date = helpers.get_random_date_between(start, end);
        let check = moment(selected_date).isBetween(start, end);

        expect(check).toBe(true);
    });

    it('should return random integer between min and max. (function: get_random_arbitrary)', () => {
        let min = 20;
        let max = 60;
        let selected_integer = helpers.get_random_arbitrary(min, max);

        expect(selected_integer).toBeGreaterThanOrEqual(min);
        expect(selected_integer).toBeLessThanOrEqual(max);
    });

    it('should return product list with "found" and "in_stock" flags for each product and product code. (function: get_product_summary)', () => {

        let products = consts.products_sample;
        let order = consts.orders_sample[0];
        let keys = ['code', 'found', 'in_stock'];

        let product_summary = helpers.get_product_summary(products, order);

        product_summary.forEach(product => {
            let product_keys = Object.keys(product);

            expect(product_keys).toEqual(keys);
            expect(typeof product.code).toBe('string');
            expect(typeof product.found).toBe('boolean');
            expect(typeof product.in_stock).toBe('boolean');
        });
    });

    it('should check if product_summary list has at least one product with "found" and "in_stock" flag with true as value. (function: has_valid_order)', () => {

        let products = consts.products_sample;
        let valid_order = consts.orders_sample[0];
        let invalid_order = consts.orders_sample[3];

        let product_summary_valid = helpers.get_product_summary(products, valid_order);
        let product_summary_invalid = helpers.get_product_summary(products, invalid_order);

        let check_valid = helpers.has_valid_order(product_summary_valid);
        let check_invalid = helpers.has_valid_order(product_summary_invalid);

        expect(check_valid).toBe(true);
        expect(check_invalid).toBe(false);

    });

    it('should return list of product code, only for valid orders. (function: get_valid_orders_from)', () => {
        let products = consts.products_sample;
        let order = consts.orders_sample[0];

        let product_summary = helpers.get_product_summary(products, order);
        let product_code_from_summary = helpers.get_valid_orders_from(product_summary);
        
        let valid_product_codes = product_summary.filter(product => product.found && product.in_stock).map(product => product.code);

        expect(product_code_from_summary).toEqual(valid_product_codes);
    });

});