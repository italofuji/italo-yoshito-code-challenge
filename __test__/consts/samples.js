export const products_sample = [ 
    { 
        _id: '1',
        code: '015',
        product_name: 'Awesome Cotton Bike 1',
        description: 'Unbranded',
        quantity_in_stock: 4,
        value: 250,
        other_attributes: { color: 'white', material: 'Steel' },
        __v: 0 
    },
    { 
        _id: '2',
        code: '016',
        product_name: 'Awesome Cotton Bike 2',
        description: 'Unbranded',
        quantity_in_stock: 0,
        value: 300,
        other_attributes: { color: 'white', material: 'Steel' },
        __v: 0 
    },
    { 
        _id: '3',
        code: '017',
        product_name: 'Awesome Cotton Bike 3',
        description: 'Unbranded',
        quantity_in_stock: 2,
        value: 160,
        other_attributes: { color: 'white', material: 'Steel' },
        __v: 0 
    } ,
    { 
        _id: '4',
        code: '018',
        product_name: 'Awesome Cotton Bike 4',
        description: 'Unbranded',
        quantity_in_stock: 14,
        value: 200,
        other_attributes: { color: 'white', material: 'Steel' },
        __v: 0 
    }
];

export const orders_sample = [
    {
        "code": "11031",
        "buyer_name": "pedro",
        "order_status": "novo",
        "transportation_cost": 100.00,
        "items": [
            {"code": "018", "quantity": 5, "value": 200}
        ]
    },
    {
        "code": "11032",
        "buyer_name": "pedro",
        "order_status": "entregue",
        "transportation_cost": 100.00,
        "items": [
            {"code": "015", "quantity": 2, "value": 250}
        ]
    },
    {
        "code": "11033",
        "buyer_name": "gabriel",
        "order_status": "entregue",
        "transportation_cost": 100.00,
        "items": [
            {"code": "016", "quantity": 2, "value": 300}
        ]
    },
    {
        "code": "11034",
        "buyer_name": "gabriel",
        "order_status": "novo",
        "transportation_cost": 100.00,
        "items": [
            {"code": "016", "quantity": 2, "value": 300}
        ]
    }
];