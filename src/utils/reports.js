import * as R from 'ramda';

/*
    Params: orders (list of orders (Objects same schema orders structure)) 
    Returns: Object with: {
        total sales: integer,
        quantity_sales: integer,
        quantity_clients: integer
    }
*/
export const get_ticket_medio_from_orders = (orders) => {
    let total_sales = R.sum(
        R.map((order) => order.items.reduce((sum, item) => (item.quantity * item.value) + sum, 0)
    , orders));
    let quantity_sales = orders.length;
    let quantity_clients = R.uniq(orders.map(order => order.buyer_name)).length;

    return {
        ticket_medio_vendas: (total_sales / quantity_sales),
        ticket_medio_clientes: (total_sales / quantity_clients)
    }
}