import * as R from 'ramda';
import moment from 'moment';

/*
    params: list
    Returns: Select one element from list and return it.
*/

export const get_random_element_from = (list) => {
    return list[Math.floor( Math.random() * list.length )];
}

/*
    Params: start (String YYYY-MM-dd), end (String YYYY-MM-dd)
    Returns: Random Date between start and end dates.
*/

export const get_random_date_between = (start_date, end_date) => {
    const start = moment(start_date);
    const end = moment(end_date);

    return new Date(start.valueOf() + Math.random() * (end.valueOf() - start.valueOf()));
}

/*
    Params: min (integer), max: (integer)
    Returns: Random integer between min and max
*/
export const get_random_arbitrary = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min);
}

/*
    Params: find_result (object), order: (object)
    Returns: Create a List of product

    {
        code: product code
        found: true if product is found
        in_stock: true if product stock has suficient quantity to attend the order
    }
*/
export const get_product_summary = (products, order) => products.map(product => {

    const order_items_code = order.items.map(item => item.code);

    // Contains list of integer, each integer is quantity of product requested
    let order_quantity = 
        order.items
            .filter(item => item.code === product.code)
            .map(item => item.quantity)[0];

    return {
            code: product.code,
            found: R.includes(product.code, order_items_code), // true if product is registered
            in_stock: product.quantity_in_stock > order_quantity // true if product has sufficient quantity on stock                                                   
    };
});

/*
    Params: product_summary (list of object)
    Returns: true if there is, at least, one product that has in stock and has suficient quantity to attend the order checking the flags
*/
export const has_valid_order = (product_summary) => {
    return product_summary.filter(item => item.found && item.in_stock).length > 0
}

/*
    Params: product_summary (list of object)
    Returns: only valid orders from product summary
*/
export const get_valid_orders_from = (product_summary) => (
    product_summary
        .filter(product => (product.found && product.in_stock))
        .map(product => product.code)
);
