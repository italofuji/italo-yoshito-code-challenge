import app from './config/app';
import mongoose from 'mongoose';

// Credentials Mongodb
import * as config from './config/_config';

console.log("Connecting to MongoDB Atlas ...");
// Connect to database (Mongodb Atlas - Cloud)
mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true }).then((result) => {
    // If connection success, start the application
    console.log('connected to mongodb atlas');
    app.listen(process.env.PORT || 3000);
    console.log('server started on http://localhost:3000');
}).catch((error) => {
    console.log(error);
});
