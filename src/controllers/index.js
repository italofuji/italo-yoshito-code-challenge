import create_product from './Products/create_product'
import get_products from './Products/get_products';
import update_product from './Products/update_product';
import delete_product from './Products/delete_product';

import create_order from './Orders/create_order';
import get_orders from './Orders/get_orders';
import update_order from './Orders/update_order';

import ticket_medio from './Reports/ticket_medio';

export default {
    create_product,
    get_products,
    update_product,
    delete_product,

    create_order,
    get_orders,
    update_order,

    ticket_medio
}