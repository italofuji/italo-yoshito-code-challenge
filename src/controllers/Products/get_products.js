import Products from '../../models/products';

/*
    Get All Products with pagination (each page has 10 documents limit)

    URL parameter pattern: /get_products/<page-number>
    HTTP Method: GET
*/

const get_products = (req, res) => {
    
    const options = {
        page: req.params.page,
        limit: 10,
    }

    Products.paginate({}, options)
        .then(result => {
            res.send(result);
        })
        .catch((error) => {
            res.send({status: "ERROR", message: error});
        });
};

export default get_products;