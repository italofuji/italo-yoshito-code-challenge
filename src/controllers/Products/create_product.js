import Product from '../../models/products';

/*
    Create new Product

    URL parameter pattern: /create_products
    HTTP Method: POST
    Body Parameter structure: {
        code
        product_name
        description
        quantity_in_stock
        value
        other_attributes 
    }
*/
const create_products = (req, res) => {
    // Check if Body was passed as body parameter
    if (req.body) {

        const product_data = req.body;
        const product = new Product(product_data);
        
        // Salve document to database
        product.save()
            .then(result => {
                res.send({status: "OK", message: "Product Created."});
            })
            .catch(error => {
                res.send({status: "ERROR", message: error});
            });
        
    } else {
        res.send({status: "ERROR", message: "You must pass product as parameter."});
    }    
}

export default create_products;