import Product from '../../models/products';

/*
    Delete a Product

    URL parameter pattern: /delete_product/<product-code>
    HTTP Method: DELETE

*/

const delete_product = (req, res) => {

    const product_code = req.params.code;

    Product.deleteOne({code: product_code}, (error) => {
        if (!error) {
            res.send({status: "OK", message: "Product Deleted Successfully"});
        } else {
            res.send({status: "ERROR", message: error});
        }
    });
}

export default delete_product;