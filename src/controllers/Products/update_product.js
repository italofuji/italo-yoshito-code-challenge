import Product from '../../models/products';
/*
    Update a Product by it's code with received Body parameters (all fields optional)

    URL parameter pattern: /update_product/<product-code>
    HTTP Method: PUT
    Body parameter: {
        code
        product_name
        description
        quantity_in_stock
        value
        other_attributes 
    }

*/
const update_product = (req, res) => {

    const product_code = req.params.code;
    const new_fields = req.body;
    const options = {
        new: true // tells mongoose to return updated data after update operation
    }
    
    // Check if product code was passed as url parameter
    if (product_code) {
        Product.findOneAndUpdate({code: product_code}, new_fields, options, (error, result) => {
            if (!error) {
                res.send(result);
            } else {
                res.send({status: "ERROR", message: error})
            }
        });       
    } else {
        res.send({status: "ERROR", message: "You must pass product code as URL parameters. (pattern: /update_product/<code here>)"})
    }    
}

export default update_product;