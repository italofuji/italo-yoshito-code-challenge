import Orders from '../../models/orders';
import moment from 'moment';
import * as R from 'ramda';
import * as reports from '../../utils/reports';
/*

Volume Total de Vendas no período, ou Venda Totais (VT)
Número de Clientes que Compraram nesse período (C)
Número de Vendas Realizadas (V)


Ticket medio de vendas com base em número de vendas:

ticket_medio_vendas = total_sales / quantity_sales



Ticket médio de vendas com base em número de clientes:


ticket_medio_clientes = total_sales / quantity_clients
*/

const ticket_medio = (req, res) => {

    const start_date = new Date(req.params.start_date);
    const end_date = new Date(req.params.end_date);

    const m_start =moment(start_date);
    const m_end = moment(end_date);

    if (m_start.isValid() && m_end.isValid()) {

        Orders.find({})
            .select({ "buyer_name": 1, "_id": 0, "items": 1, "order_status": 1})
            .where('purchase_date').gt(start_date).lt(end_date)
            .where('order_status').equals('aprovado')
            .then((orders) => {

                let { ticket_medio_vendas, ticket_medio_clientes } = reports.get_ticket_medio_from_orders(orders);

                res.send({
                    status: "OK",
                    ticket_medio_vendas,
                    ticket_medio_clientes
                });
            })
            .catch(error => {
                res.send({status: "ERROR", message: "Error when trying to find orders."});
            });
    } else {
        res.send({status:"ERROR", message: "Please pass start date and end date as URL parameter. (/ticket_medio/<start-date>/<end-date>)"});
    }

}

export default ticket_medio;