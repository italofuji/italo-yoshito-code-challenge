import Order from '../../models/orders';

const update_order = (req, res) => {
    const order_code = req.params.code;
    const new_fields = req.body;
    const options = {
        new: true // tells to mongoose to return updated data after update operation
    }

    if (order_code) {
        Order.findOneAndUpdate({code: order_code}, new_fields, options, (error, result) => {
            if (!error) {
                res.send(result);
            } else {
                res.send({status: "ERROR", message: error})
            }
        });       
    } else {
        res.send({status: "ERROR", message: "You must pass order code as URL parameters. (pattern: /update_product/<code here>)"})
    }
}

export default update_order;