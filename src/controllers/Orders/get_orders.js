import Order from '../../models/orders';

const get_orders = (req, res) => {
    const options = {
        page: req.params.page,
        limit: 10,
    }

    Order.paginate({}, options)
        .then(result => {
            res.send(result);
        })
        .catch((error) => {
            res.send({status: "ERROR", message: error});
        });
}

export default get_orders;