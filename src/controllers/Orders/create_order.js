import Product from '../../models/products';
import Order from '../../models/orders';
import * as R from 'ramda';
import * as helpers from '../../utils/geral_purpose_helpers';

const create_order = (req, res) => {

    const order = req.body;    
    const order_items_code = order.items.map(item => item.code);
    const options = {
        new: true // tells to mongoose to return updated data after update operation
    }

    Product.find({ code: { $in: order_items_code } })
        .lean()
        .then(result => {
            console.log(result);
            // Create a list of items order with some flags (found and in_stock flags)
            const product_order_summary = helpers.get_product_summary(result, order, order_items_code);
            console.log(product_order_summary);
            // Verify if it has valid order
            if (helpers.has_valid_order( product_order_summary )) {

                const possible_orders = helpers.get_valid_orders_from( product_order_summary );

                // Update items to only items that are possible to order
                order.items = order.items.filter(item => R.includes(item.code, possible_orders));

                // Decrement found product on Stock
                order.items.map(item => {
                    Product.findOneAndUpdate({code: item.code}, { $inc: { quantity_in_stock: -item.quantity } }, options, (error, result) => {
                        if (error) {
                            res.send({status: "ERROR", message: `DECREMENT FAILED: ${error}`})
                        }
                    });
                });

                // Create Orders
                Order.insertMany(order, (error, docs) => {
                    if (!error) {
                        res.send(product_order_summary);
                    } else {
                        res.send({status: "ERROR", message: `CREATE ORDER FAILED: ${error}`});
                    }
                });

            } else {
                res.send({status: "ERROR", message: "Products not found or there is not sufficient quantity on none of your requested items in stock."});
            }   
        })
        .catch(error => {
            res.send({status: "ERROR", message: `Error when tried to find product in stock: ${error}`});
        });

}

export default create_order;