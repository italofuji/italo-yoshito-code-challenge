import controllers from '../controllers'

const routes = (app) => {

    // Products Endpoints
    app.post("/create_product", controllers.create_product);
    app.get("/get_products/:page", controllers.get_products);
    app.put("/update_product/:code", controllers.update_product);
    app.delete("/delete_product/:code", controllers.delete_product);

    // Order Endpoints
    app.post("/create_order", controllers.create_order);
    app.get("/get_orders/:page", controllers.get_orders);
    app.put("/update_order/:code", controllers.update_order);

    // Report
    app.get("/ticket_medio/:start_date/:end_date", controllers.ticket_medio);

}   
    

export default routes;