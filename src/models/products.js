import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const productSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
    },
    product_name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    quantity_in_stock: {
        type: Number,
        required: true
    },
    value: {
        type: Number,
        required: true
    },
    other_attributes: {
        type: Object,
        required: false
    }
});

productSchema.plugin(mongoosePaginate);

export default mongoose.model('Product', productSchema);