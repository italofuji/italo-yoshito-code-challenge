import mongoose, { Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const orderSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
    },
    purchase_date: {
        type: Date,
        default: Date.now
    },
    buyer_name: {
        type: String,
        required: true
    },
    order_status: {
        type: String,
        required: true,
        enum: ['novo','aprovado','entregue', 'cancelado']
    },
    transportation_cost: {
        type: Number,
        required: true
    },
    items: {
        type: [
            {
                code: {
                    type:   String,
                    required: true,
                },
                quantity: {
                    type: Number,
                    required: true
                },
                value: {
                    type: Number,
                    required: true
                }
            }
        ],
        validade: {
            validator: (list) => list.length > 0,
            message: "Inform at least one item."
        }
    }
});

orderSchema.plugin(mongoosePaginate);

export default mongoose.model('Order', orderSchema);