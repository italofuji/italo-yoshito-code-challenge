# Tecnologias Utilizadas para o Desenvolvimento

- Node.JS (Versão: v10.14.1)
- MongoDB Atlas (Solução em Cloud)
- Express (Framework para desenvolviemnto Web)
- Jest (Framework de Testes desenvolvido pelo Facebook)

# Instalação do Projeto

Após a instalação do NodeJS:

Clone o Projeto e navegue até a raiz do projeto onde se encontra o packege.json, e rode o comando:

**npm install**

ou caso estiver o yarn instalado (outro gerenciador de pacote)

**yarn**

# Comandos Relacionado ao Projeto

## Para Rodar os Testes:

**npm run test**

ou

**yarn test**

# Geração de Novos Dados

No Projeto existe 2 scripts que geram algumas amostras de dados de pedidos e de produtos com os seguintes comandos:

**npm run generate_product_sample**

**npm run generate_order_sample**

ou 

**yarn generate_product_sample**

**yarn generate_order_sample**

## Atenção!

**Importante gerar Pedidos depois que gerar os Produtos.**


# Como Inicializar a aplicação:

**npm start**

ou

**yarn start**


# Descrição da Regra de Negócio do Projeto
# Desafio - SkyHub

Pedimos que a entrega do desafio seja realizada em até 10 dias. Caso precise de mais tempo, nos avise que podemos negociar o prazo.


## A loja do "seu" Manuel
Seu Manuel deseja expandir seus negócios e vender *online*, para isso ele te contratou para tocar o desenvolvimento do sistema que vai dar suporte à operação da loja. São listados abaixo os serviços que devem ser disponibilizados pelo sistema.

### Produtos
Serviço de produtos para que seja possível cadastrar, recuperar, atualizar e deletar os produtos de sua loja.

Os produtos serão identificados por um **código** e terá as informações de **nome**, **descrição**, **estoque**, **preço** e mais alguns **atributos** que variam de produto para produto (customizáveis).

### Pedidos
Serviço de pedidos que permitirá registrar, recuperar e atualizar as vendas dos seus produtos. As informações dos pedido consistem em um **código** identificador, **data** da compra, nome do **comprador**, **estado** (ex: novo, aprovado, entregue e cancelado), valor do **frete** e a lista de **itens** que foram vendidos - cada item possui: **código** do produto; **quantidade**; e o **preço** de venda.

### Relatórios
Por fim, o sistema também deve fornecer a possibilidade de extrair um relatório que informa o **ticket médio** dado um intervalo de tempo (data inicial e final) - a definição de ticket médio é facilmente encontrada na web, mas fique à vontade para indicar a definição utilizada na solução.

## Considerações
Considere que o sistema deve:

- Recusar a criação de um pedido com item não cadastrado;
- Decrementar o estoque do(s) produto(s) sempre que um pedido é realizado;
- Evitar o cadastro de pedidos cujo item não tem estoque suficiente.

### Observações:
- Para o desafio, pedimos apenas que desenvolva as APIs do sistema (não é necessário desenvolver as telas - frontend);
- A API deve usar dados no formato JSON para realizar as operações;
- A estrutura do JSON de cada recurso deve ser definida por você (justifique as escolhas onde achar pertinente);
- Os dados devem ser armazenados em um banco também a seu critério;
- É necessário escrever testes automáticos para os serviços;
- A escolha das ferramentas para realizar o desafio são livres, mas esperamos que você nos diga o porque usou cada uma delas;
- Implementar um procedimento de geração de dados de exemplo (*sample data*) seria um **ponto extra**!

## Critérios de avaliação
- Atendimento aos requisitos descritos;
- Legibilidade da solução;
- Cobertura dos testes;
- Eficiência - evite desperdício de recursos!

Crie um *fork* desse repositório e nos envie um **pull request**.

Não esqueça de ensinar como instalamos e rodamos seu projeto em nosso ambiente. :sunglasses:
